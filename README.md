## Usage

`docker-compose up -d --build site`

Ports required

- **nginx** - `:8080`
- **mysql** - `:3306`
- **php** - `:9000`

Other useful commands

- `docker-compose run --rm composer update`
- `docker-compose run --rm npm run dev`
- `docker-compose run --rm artisan migrate`
- `docker-compose run --rm artisan db:seed`
- `docker-compose run --rm artisan migrate:fresh --seed`
- `docker-compose run --rm artisan test`

## Persistent MySQL Storage

```
volumes:
  - ./mysql:/var/lib/mysql
```

## Source

[Create a local Laravel dev environment with Docker by Andrew Schmelyun](https://www.youtube.com/watch?v=5N6gTVCG_rw&ab_channel=AndrewSchmelyun)